module qrcode-token

go 1.17

require (
	github.com/mdp/qrterminal/v3 v3.0.0
	github.com/skip2/go-qrcode v0.0.0-20200617195104-da1b6568686e
	github.com/zf1976/vlog v1.0.3
)

require rsc.io/qr v0.2.0 // indirect
